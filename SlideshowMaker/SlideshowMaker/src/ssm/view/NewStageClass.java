/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.NEXT_BUTTON;
import static ssm.LanguagePropertyType.PREVIOUS_BUTTON;
import static ssm.StartupConstants.ICON_WINDOW;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
/**
 *
 * @author shengchun
 */
public class NewStageClass {    
    public static int i = 0;   
    
    
    public static void disPlaySlideShow( SlideShowModel slideShow){
      PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + ICON_WINDOW;
	Image windowIcon  = new Image(imagePath); 
        Stage newStage;
        Button nextButton;
        Button previousButton;   
        Scene newScene;     
        newStage = new Stage();
        newStage.getIcons().add(windowIcon);       
        newStage.setTitle(slideShow.getTitle());
        Label titleLabel = new Label();
        titleLabel.setText(slideShow.getTitle());
         
       String nextButtonLabel = props.getProperty(NEXT_BUTTON.toString());       
        nextButton = new Button(nextButtonLabel);
        String previousButtonLabel = props.getProperty(PREVIOUS_BUTTON.toString());
        previousButton= new Button(previousButtonLabel);        
        BorderPane newPane = new BorderPane();        
        newPane.setLeft(previousButton);
        newPane.setRight(nextButton);
        newPane.setTop(titleLabel);
        Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();
        newStage.setX(bounds.getMinX());
	newStage.setY(bounds.getMinY());
	newStage.setWidth(bounds.getWidth());
	newStage.setHeight(bounds.getHeight());
        Label captionLabel = new Label("");
        Slide slide = slideShow.getSlides().get(i); 
        
        Image image=new Image("file:" +slide.getImagePath() + slide.getImageFileName()); 
        ImageView slideImage = new ImageView(image);  
        slideImage.setFitHeight(400);
        slideImage.setFitWidth(500);        
        
        newPane.setCenter(slideImage); 
        
        captionLabel.setText(slide.getCatption());
        newPane.setBottom(captionLabel);
            
         nextButton.setOnAction(new EventHandler<ActionEvent>() {          
          public void handle(ActionEvent e) {              
            i=i+1;
            if(i>=(slideShow.getSlides().size()-1)){
              nextButton.setDisable(true);    
              previousButton.setDisable(false);
            }            
            Slide slideNext = slideShow.getSlides().get(i);
            captionLabel.setText(slideNext.getCatption());
            Image nextImage=new Image("file:" +slideNext.getImagePath() + slideNext.getImageFileName());
                  slideImage.setImage(nextImage);         
             
                   }            
      });  
        
        
       previousButton.setOnAction(e->{
             i=i-1;
             if(i<=0){
              previousButton.setDisable(true);    
               nextButton.setDisable(false);    
            }
           Slide slidePrevious = slideShow.getSlides().get(i);
            captionLabel.setText(slidePrevious.getCatption());
            Image previousImage=new Image("file:" +slidePrevious.getImagePath() + slidePrevious.getImageFileName());
            slideImage.setImage(previousImage);     
           
    });      
           
        
	newScene = new Scene(newPane);	
       	newScene.getStylesheets().add(STYLE_SHEET_UI);
	newStage.setScene(newScene);
	newStage.show();
    
    
    }
       
     
     
    }


     
    