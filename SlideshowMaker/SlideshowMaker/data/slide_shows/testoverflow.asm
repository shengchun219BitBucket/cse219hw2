.text
.globl main
main:
character:
# ask the user to input character1 and save it in t0
la $a0, character1
li $v0,4
syscall 
li $v0,12
syscall
move $t0,$v0
move $a0, $t0
li $v0,1
syscall 
# ask the user to input character2 and save it in t1
la $a0, character2
li $v0,4
syscall
li $v0, 12
syscall
move $t1,$v0
move $a0, $t1
li $v0,1
syscall 
# ask the user to input character3 and save it in t2
la $a0, character3
li $v0,4
syscall
li $v0, 12
syscall
move $t2,$v0
move $a0, $t2
li $v0,1
syscall 
# ask the user to input character4 and save it in t3
la $a0, character4
li $v0,4
syscall
li $v0, 12
syscall
move $t3,$v0
move $a0, $t3
li $v0,1
syscall 
move $a0,$t0  # print a0 in binary 
li $v0,35
syscall 
la $a0,endl
li $v0,4
syscall
sll $t1,$t1,8  # move t1 and print 
move $a0,$t1
li $v0,35
syscall 
la $a0,endl
li $v0,4
syscall
sll $t2,$t2,16   #move t2 and print
move $a0,$t2
li $v0,35
syscall 
la $a0,endl  
li $v0,4
syscall
sll $t3,$t3, 24  #move t3 and print
move $a0,$t3
li $v0,35
syscall 
la $a0, endl
li $v0,4
syscall
or $t5,$t0,$t1     
move $a0, $t5
li $v0, 35
syscall 
or $t5,$t5,$t2
la $a0, endl
li $v0,4
syscall
move $a0, $t5
li $v0, 35
syscall
or $t5,$t5,$t3
la $a0, endl
li $v0,4
syscall
move $a0, $t5
li $v0, 1
syscall



.data
character1: .asciiz "Enter the first character: "
character2: .asciiz "Enter the second character: "
character3: .asciiz "Enter the third character: "
character4: .asciiz "Enter the fourth character: "

space: .asciiz "              "
endl:.asciiz "\n"
